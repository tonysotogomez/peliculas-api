<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make('tonysoto');

        App\User::insert([
            'name' => 'Tony',
            'lastname' => 'Soto',
            'email' => 'tonysotogomez@gmail.com',
            'birth' => '1991-10-16',
            'password' => $password,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}
