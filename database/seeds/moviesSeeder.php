<?php

use Illuminate\Database\Seeder;

class moviesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        App\Movie::insert([
            'name' => 'Ironman',
            'description' => 'Es la primera entrega del Universo cinematográfico de Marvel. La cinta fue dirigida por Jon Favreau, con un guion de Stan Lee, Hawk Ostby, Art Marcum y Matt Holloway. Es protagonizada por Robert Downey Jr, Terrence Howard, Jeff Bridges, Shaun Toub y Gwyneth Paltrow. La trama gira en torno a Tony Stark, un empresario e ingeniero, que construye un exoesqueleto motorizado y se convierte en Iron Man, un superhéroe tecnológicamente avanzado.',
            'release' => '2008',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

        App\Movie::insert([
            'name' => 'Batman Inicia',
            'description' => 'La película reinicia la serie fílmica de Batman, contando la historia de origen del personaje principal, desde el miedo inicial a los murciélagos de su álter ego Bruce Wayne, pasando por la muerte de sus padres y su viaje para convertirse en Batman, hasta su lucha para evitar que Ras al Ghul y el Espantapájaros destruyan Ciudad Gótica.',
            'release' => '2005',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}
