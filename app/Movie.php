<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
	use SoftDeletes;
	protected $table="movies";
	protected $perPage = 3;

	protected $fillable = array('name','description','release');
	
	protected $hidden = ['created_at','updated_at','deleted_at']; 
}
