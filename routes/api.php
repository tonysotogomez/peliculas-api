<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');


Route::group(['middleware' => 'auth:api'],function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('movies', 'MovieController@index');
    Route::get('movies/{movie}', 'MovieController@show');
    Route::post('movies/buscar', 'MovieController@buscar');
    Route::post('movies', 'MovieController@store');
    Route::put('movies/{movie}', 'MovieController@update');
    Route::delete('movies/{id}', 'MovieController@destroy');
});
